import { Router} from 'express';
const router = Router();

import { createVideo, getVideo, getOneVideo, deleteVideo, updateVideo } from '../controllers/video.controller';
// /api/projects/
router.post('/', createVideo);
router.get('/', getVideo);

// /api/videos/:videoId
router.get('/:id', getOneVideo);
router.delete('/:id', deleteVideo);
router.put('/:id', updateVideo);
export default router;