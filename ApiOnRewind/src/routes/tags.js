import { Router} from 'express';
const router = Router();

import { createTag, getTag, getOneTag, deleteTag, updateTag } from '../controllers/tag.controller';
// /api/projects/
router.post('/', createTag);
router.get('/', getTag);

// /api/tags/:tagId
router.get('/:id', getOneTag);
router.delete('/:id', deleteTag);
router.put('/:id', updateTag);
export default router;