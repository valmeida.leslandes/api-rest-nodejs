import Sequelize from 'sequelize';
import { sequelize } from '../database/database';
import Video from './Videos';
import Tag from './Tags';

const Possede = sequelize.define('Possede', {
    id_video: {
        type: Sequelize.INTEGER,
        primaryKey: true
    },
    id_tag: {
        type: Sequelize.INTEGER,
        primaryKey: true    }
}, {
    timestamps: false
});

export default Possede;