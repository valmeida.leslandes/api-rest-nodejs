import Sequelize from 'sequelize';
import { sequelize } from '../database/database';
 
const Video = sequelize.define('videos', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true
    },
    nom: {
        type: Sequelize.STRING
    },
    description: {
        type: Sequelize.STRING
    },
    url: {
        type: Sequelize.STRING
    },
    createdat: {
        type: Sequelize.DATE
    },
    updatedat: {
        type: Sequelize.DATE
    }
}, {
    timestamps: false
});
Video.associate = function(models) {
    Video.belongsToMany(models.Tag, {
        through: 'Possede',
        as: 'tags',
        foreignKey: 'id',
        otherKey: 'id'
    });
};

export default Video;