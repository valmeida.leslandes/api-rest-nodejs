import Sequelize from 'sequelize';
import { sequelize } from '../database/database';

const Tag = sequelize.define('tags', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true
    },
    valeur: {
        type: Sequelize.INTEGER
    }
}, {
    timestamps: false
});

Tag.associate = function(models) {
    Tag.belongsToMany(models.Video, {
        through: 'Possede',
        as: 'videos',
        foreignKey: 'id',
        otherKey: 'id'
    });
};

export default Tag;