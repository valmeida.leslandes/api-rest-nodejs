import express, {json} from 'express';
import morgan from 'morgan';

// Importing routes
import videoRoutes from './routes/videos';
import tagRoutes from './routes/tags';
// initialization
const app = express();

//middlewares
app.use(morgan('dev'));
app.use(json());

// routes
app.use('/api/videos',videoRoutes);
app.use('/api/tags',tagRoutes);


export default app;