import Tag from '../models/Tags';

export async function getTag(req,res) {
    try {
        
        const tags = await Tag.findAll();
        res.json({
            data: tags
        });
    } catch(e) {
        console.log(e);
    }
}

export async function createTag(req,res) {
    const { valeur } = req.body;
    try {
        let newTag = await Tag.create({
            valeur
        }, {
            fields: ['valeur']
        });
        if (newTag) {
            return res.json({
                message: 'Tag created successfully',
                data: newTag
            });
        }
    } catch(error) {
        console.log(error);
        res.status(500).json({
            message:'Error',
            data: {}
        });
    }
};

export async function getOneTag(req,res) {
    const { id } = req.params;
    const tag = await Tag.findOne({
        where: {id}
    });
    res.json(tag);
};

export async function deleteTag(req,res) {
    const { id } = req.params;
    const deleteRowCount = await Tag.destroy({
        where: {
            id
        }
    });
    res.json({
        message: 'Tag Deleted succesfully',
        count: deleteRowCount
    });
};

export async function updateTag(req,res) {
    const { id } = req.params;
    const { valeur } = req.body;

    const tags = await Tag.findAll({
        attributes: ['id','valeur'],
        where: {
            id
        }
    });

    if(tags.length > 0) {
        tags.forEach(async Tag => {
            await Tag.update({
                valeur
            });
        })
    }

    return res.json({
        message : 'Tag Updated Succesfully',
        data: tags
    });
};
