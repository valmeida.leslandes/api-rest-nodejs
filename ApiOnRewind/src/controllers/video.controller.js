import Video from '../models/Videos';

export async function getVideo(req,res) {
    try {
        
        const videos = await Video.findAll();
        res.json({
            data: videos
        });
    } catch(e) {
        console.log(e);
    }
}

export async function createVideo(req,res) {
    const { nom, description, url, createdat, updatedat} = req.body;
    try {
        let newVideo = await Video.create({
            nom,
            description,
            url,
            createdat,
            updatedat
        }, {
            fields: ['nom', 'description', 'url', 'createdat','updatedat']
        });
        if (newVideo) {
            return res.json({
                message: 'Video created successfully',
                data: newVideo
            });
        }
    } catch(error) {
        console.log(error);
        res.status(500).json({
            message:'Error',
            data: {}
        });
    }
};

export async function getOneVideo(req,res) {
    const { id } = req.params;
    const video = await Video.findOne({
        where: {id}
    });
    res.json(video);
};

export async function deleteVideo(req,res) {
    const { id } = req.params;
    const deleteRowCount = await Video.destroy({
        where: {
            id
        }
    });
    res.json({
        message: 'Video Deleted succesfully',
        count: deleteRowCount
    });
};

export async function updateVideo(req,res) {
    const { id } = req.params;
    const { nom, description, url, createdat, updatedat } = req.body;

    const videos = await Video.findAll({
        attributes: ['id','nom','description','url','createdat','updatedat'],
        where: {
            id
        }
    });

    if(videos.length > 0) {
        videos.forEach(async Video => {
            await Video.update({
                nom, 
                description,
                url,
                createdat,
                updatedat
            });
        })
    }

    return res.json({
        message : 'Video Updated Succesfully',
        data: videos
    });
};
