CREATE DATABASE BDDOlivier;

CREATE TABLE IF NOT EXISTS videos(
    id SERIAL PRIMARY KEY,
    nom VARCHAR NOT NULL,
    description VARCHAR,
    url VARCHAR NOT NULL, 
    createdat DATE NOT NULL, 
    updatedat DATE
);

CREATE TABLE IF NOT EXISTS tags(
    id SERIAL PRIMARY KEY,
    valeur INTEGER NOT NULL UNIQUE
);

CREATE TABLE IF NOT EXISTS possede(
    id_video INTEGER REFERENCES videos(id),
    id_tag INTEGER REFERENCES tags(id)
);

INSERT INTO videos(
    nom,
    description,
    url,
    createdat,
    updatedat
) VALUES('GOTAGOFAST','video tres rapide','http://youtube.com','2000-02-03','2000-03-04');

INSERT INTO tags(
    valeur
) VALUES('300');

INSERT INTO possede(
    id_video,id_tag
) VALUES(1,1);