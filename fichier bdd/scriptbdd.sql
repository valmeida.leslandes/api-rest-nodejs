------------------------------------------------------------
--        Script Postgre 
------------------------------------------------------------



------------------------------------------------------------
-- Table: Videos
------------------------------------------------------------
CREATE TABLE public.Videos(
	id            SERIAL NOT NULL ,
	nom           VARCHAR (25) NOT NULL ,
	description   VARCHAR (25)  ,
	url           VARCHAR (25) NOT NULL ,
	createdAt     DATE   ,
	updatedAt     DATE    ,
	CONSTRAINT Videos_PK PRIMARY KEY (id)
)WITHOUT OIDS;


------------------------------------------------------------
-- Table: Tags
------------------------------------------------------------
CREATE TABLE public.Tags(
	id       SERIAL NOT NULL ,
	valeur   INT  NOT NULL  ,
	CONSTRAINT Tags_PK PRIMARY KEY (id) ,
	CONSTRAINT Tags_AK UNIQUE (valeur)
)WITHOUT OIDS;


------------------------------------------------------------
-- Table: possede
------------------------------------------------------------
CREATE TABLE public.possede(
	id_video        INT  NOT NULL ,
	id_tag   INT  NOT NULL  ,
	CONSTRAINT possede_PK PRIMARY KEY (id_video,id_tag)

	,CONSTRAINT possede_Videos_FK FOREIGN KEY (id_video) REFERENCES public.Videos(id)
	,CONSTRAINT possede_Tags0_FK FOREIGN KEY (id_tag) REFERENCES public.Tags(id)
)WITHOUT OIDS;



